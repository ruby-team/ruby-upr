ruby-upr (0.3.0-3) unstable; urgency=medium

  * Team upload

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Cédric Boutillier ]
  * [ci skip] Update team name
  * [ci skip] Add .gitattributes to keep unwanted files
    out of the source package

  [ Utkarsh Gupta ]
  * Tighten dependency on ruby-moneta
  * Add patch to update moneta (Closes: #883370)
  * Fix package wrt cme

 -- Utkarsh Gupta <utkarsh@debian.org>  Fri, 05 Feb 2021 23:40:18 +0530

ruby-upr (0.3.0-2) unstable; urgency=low

  * Rebuild for Ruby versions currently in the archive.
  * Bump Standards-Version, no changes required.

 -- Jérémy Bobbio <lunar@debian.org>  Sat, 03 May 2014 15:31:55 +0200

ruby-upr (0.3.0-1) unstable; urgency=low

  * New upstream version:
    - Drop 0001-Update-Upr-Monitor-to-use-Moneta-0.7-API.patch, merged
      upstream.

 -- Jérémy Bobbio <lunar@debian.org>  Wed, 07 Aug 2013 12:12:21 +0200

ruby-upr (0.2.0-2) unstable; urgency=low

  [ Cédric Boutillier ]
  * Use canonical URI in Vcs-* fields.

  [ Jérémy Bobbio ]
  * Fix FTBFS by switching to the 0.7 API of Moneta. (Closes: #713157)

 -- Jérémy Bobbio <lunar@debian.org>  Sun, 23 Jun 2013 13:14:39 +0200

ruby-upr (0.2.0-1) unstable; urgency=low

  * Initial release (Closes: #703105)

 -- Jérémy Bobbio <lunar@debian.org>  Mon, 18 Mar 2013 16:33:34 +0100
