# -*- encoding: binary -*-
require 'rack'

module Upr

  module Params

    # we'll add compatibility for existing upload progress modules
    # we find here, but under no circumstances will we help
    # proliferate new and subtly incompatible mechanisms.
    # X-Progress-ID is used in both lighttpd and nginx (3rd party module)
    # "upload_id" is used by mongrel_upload_progress
    def extract_upload_id(env)
      upid = env['HTTP_X_PROGRESS_ID'] and return upid

      # can't blindly parse params here since we don't want to read
      # the POST body if there is one, so only parse stuff in the
      # query string...
      params = Rack::Request.new(env).GET
      env["upr.upload_id"] = params["X-Progress-ID"] || params["upload_id"]
    end

  end
end
