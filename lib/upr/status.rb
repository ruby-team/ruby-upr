# -*- encoding: binary -*-
require 'upr/status_methods'
module Upr

  # this is what we store in the Moneta-backed monitor
  class Status < Struct.new(:seen, :length)
    include StatusMethods
  end
end
