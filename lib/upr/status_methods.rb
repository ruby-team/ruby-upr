# -*- encoding: binary -*-
module Upr
  # mixin module for both Upr::Status and UprStatus (AR example module)
  module StatusMethods
    def error?
      seen < 0
    end

    def done?
      length && seen >= length
    end
  end
end
